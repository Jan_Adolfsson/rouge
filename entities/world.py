from random import Random
from .chars import FLOOR, STONE

class World:
    def __init__(self, x, y, w, h, cells):
        self.cells = cells
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def get(self, x, y):
        return self.cells[y][x]


def rand_pos(world):
    rand = Random()
    found = False
    cell = None
    while not found:
        x = rand.randrange(0, world.w)
        y = rand.randrange(0, world.h)

        cell = world.get(x,y)
        found = cell == FLOOR
            
    return (x, y)


