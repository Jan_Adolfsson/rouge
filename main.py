from entities.hero import Hero
from entities.enemy import Enemy
from entities.treasure import Treasure
from bearlibterminal import terminal
from draw.lines import draw_line_rect, LineThickness
from dungeon.ran_walk_gen import RanWalkGen
from systems.draw import draw_world, draw_hero, draw_enemy, draw_treasure, draw_text
from entities.world import rand_pos, World
from input import keys
from input.handler import create_cmd
from collections import namedtuple


def setup_terminal(w, h):
    terminal.open()
    terminal.set("window: fullscreen=true;")
    terminal.set("window: size={}x{};".format(w,h))


def create_world(x, y, w, h, iters, min_iter):
    generator = RanWalkGen()
    generator.generate(w, h, iters)
    while generator.get_iteration() > min_iter:
        generator.generate(w, h, iters)

    cells = generator.get_world()
    world = World(x, y, w, h, cells)
    
    return world


def create_hero(world):
    hero = Hero()
    (x, y) = rand_pos(world)
    hero.x = x
    hero.y = y

    return hero

def create_enemy(world):
    enemy = Enemy()
    (x, y) = rand_pos(world)
    enemy.x = x
    enemy.y = y

    return enemy


def create_treasure(world):
    treasure = Treasure()
    (x, y) = rand_pos(world)
    treasure.x = x
    treasure.y = y

    return treasure


# setup
width = 240
height = 66
setup_terminal(width, height)

iters = 500
min_iters = 200
offs_x = offs_y = 1
world = create_world(offs_x, offs_y, width - 2, height - 2, iters, min_iters)
hero = create_hero(world)
enemy = create_enemy(world)
treasure = create_treasure(world)

# game loop
key = terminal.read()
while key != keys.QUIT:

    # input
    print(key)
    key = terminal.read()
    cmd = create_cmd(key)

    # update
    cmd(world, hero)
    if hero.x == treasure.x and hero.y == treasure.y:
        draw_text(world.x, world.y, 0, 30, "DUNGEON DONE!")
        break

    # draw
    draw_line_rect(0, 0, width, height, LineThickness.THICK)
    draw_world(world)
    draw_hero(hero, world.x, world.y)
    draw_enemy(enemy, world.x, world.y)
    draw_treasure(treasure, world.x, world.y)    
    terminal.refresh()

terminal.close()

