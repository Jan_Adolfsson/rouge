import entities.chars as chrs


def left(world, mv):
    if mv.x == 0:
        return

    cell = world.get(mv.x - 1, mv.y)
    if cell == chrs.STONE:
        return

    mv.x -= 1


def up(world, mv):
    if mv.y == 0:
        return

    cell = world.get(mv.x, mv.y - 1)
    if cell == chrs.STONE:
        return

    mv.y -= 1


def right(world, mv):
    if mv.x == world.w - 1:
        return

    cell = world.get(mv.x + 1, mv.y)
    if cell == chrs.STONE:
        return


    mv.x += 1


def down(world, mv):
    if mv.y == world.h - 1:
        return

    cell = world.get(mv.x, mv.y + 1)
    if cell == chrs.STONE:
        return


    mv.y += 1