from bearlibterminal import terminal

def draw_world(world):
    
    for y in range(world.h):
        for x in range(world.w):
            terminal.printf(world.x + x, world.y + y, "[color=gray]"+world.get(x,y))

def draw_hero(hero, offs_x, offs_y):
    terminal.printf(offs_x + hero.x, offs_y + hero.y, "[color=white]" + hero.char)

def draw_enemy(enemy, offs_x, offs_y):
    terminal.printf(offs_x + enemy.x, offs_y + enemy.y, "[color=red]" + enemy.char)    

def draw_treasure(treasure, offs_x, offs_y):
    terminal.printf(offs_x + treasure.x, offs_y + treasure.y, "[color=yellow]" + treasure.char)        

def draw_text(offs_x, offs_y, x, y, text):
    terminal.printf(offs_x + x, offs_y + y, "[color=white] [TK_ALIGN_CENTER]" + text)        