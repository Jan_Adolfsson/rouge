import random
import enum

North = 0
North_East = 1
East = 2
South_East = 3
South = 4
South_West = 5
West = 6
North_West = 7

STONE = "#"
FLOOR = "."

class RanWalkGen:
    def __init__(self):
        self._world = list()
        self._x = -1
        self._y = -1
        self._width = -1
        self._height = -1
        self._stuck = False

    def _is_cell_visited(self, x, y):
        return self._world[y][x] == FLOOR

    def _can_walk_to(self, dir):

        if dir == North:
            return self._y > 0 and not self._is_cell_visited(self._x, self._y - 1)

        elif dir == East:
            return self._x < self._width - 1 and not self._is_cell_visited(self._x + 1, self._y)

        elif dir == South:
            return self._y < self._height - 1 and not self._is_cell_visited(self._x, self._y + 1)

        elif dir == West:
            return self._x > 0 and not self._is_cell_visited(self._x - 1, self._y)

        else:
            raise Exception("Illegal direction.")

    def _choose_dir(self):
        directions = list([North, East, South, West])

        dir = random.choice(directions)
        while not self._can_walk_to(dir):
            directions.remove(dir)

            if len(directions) == 0:
                self._stuck = True
                return None

            dir = random.choice(directions)

        return dir

    def _create_row_of_walls(self):
        row = []
        for i in range(self._width):
            row.append(STONE)

        return row

    def _turn_cell_to_floor(self):
        self._world[self._y][self._x] = FLOOR

    def _walk_north(self):
        self._y -= 1
        self._turn_cell_to_floor()

    def _walk_east(self):
        self._x += 1
        self._turn_cell_to_floor()

    def _walk_south(self):
        self._y += 1
        self._turn_cell_to_floor()

    def _walk_west(self):
        self._x -= 1
        self._turn_cell_to_floor()

    def _walk(self, dir):
        if dir == North:
            self._walk_north()
        elif dir == East:
            self._walk_east()
        elif dir == South:
            self._walk_south()
        elif dir == West:
            self._walk_west()
        else:
            raise Exception("Illegal direction.")

    def _step(self):
        dir = self._choose_dir()
        if not self._stuck:
            self._walk(dir)

    def _init_world(self):
        self._world = list()
        for y in range(self._height):
            row = list()
            for x in range(self._width):
                cell = STONE
                row.append(cell)

            self._world.append(row)

    def _init_pos(self):
        self._x = random.randint(0, self._width - 1)
        self._y = random.randint(0, self._height - 1)

    def generate(self, width, height, iterations):
        self._width = width
        self._height = height
        self._iterations = iterations

        self._init_world()
        self._init_pos()
        self._stuck = False

        while self._iterations and not self._stuck:
            self._step()
            self._iterations -= 1

    def get_world(self):
        return self._world

    def get_iteration(self):
        return self._iterations
