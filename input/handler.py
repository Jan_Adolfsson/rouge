
from .keys import N, E, S, W
#from ..systems.move import up, down, left, right
import systems.move as mv

def mv_north(world, hero):
    mv.up(world, hero)

def mv_south(world, hero):
    mv.down(world, hero)

def mv_west(world, hero):
    mv.left(world, hero)

def mv_east(world, hero):
    mv.right(world, hero)            

def nothing(world, hero):
    pass

def create_cmd(key):
    if key == N:
        return mv_north
    elif key == E:
        return mv_east
    elif key == S:
        return mv_south
    elif key == W:
        return mv_west
    else:
        return nothing
