from enum import Enum
from bearlibterminal import terminal


class LineThickness(Enum):
    MEDIUM = 0,
    THICK = 1,
    DOUBLE = 2


class Line(Enum):
    H = 0,
    V = 1,
    U_L = 2,
    U_R = 3,
    B_L = 4,
    B_R = 5


def get_medium_lines():
    return {
        Line.H: "\u2500",
        Line.V: "\u2502",
        Line.U_L: "\u250c",
        Line.U_R: "\u2510",
        Line.B_L: "\u2514",
        Line.B_R: "\u2518"}


def get_thick_lines():
    return {
        Line.H: "\u2501",
        Line.V: "\u2503",
        Line.U_L: "\u250f",
        Line.U_R: "\u2513",
        Line.B_L: "\u2517",
        Line.B_R: "\u251b"}


def get_dbl_lines():
    return {
        Line.H: "\u2550",
        Line.V: "\u2551",
        Line.U_L: "\u2554",
        Line.U_R: "\u2557",
        Line.B_L: "\u255a",
        Line.B_R: "\u255d"}


def get_lines(thickness):
    if thickness == LineThickness.MEDIUM:
        return get_medium_lines()
    elif thickness == LineThickness.THICK:
        return get_thick_lines()
    else:
        return get_dbl_lines()


def get_line_chr(line, thickness):
    lines = get_lines(thickness)

    return lines.get(line, " ")


def draw_full_height(x, char, pad=0):
    h = get_height()

    for y in range(pad, h - (2*pad)):
        terminal.printf(x, y, char)


def draw_full_width(y, char, pad=0):
    w = get_width()

    for x in range(pad, w - (2*pad)):
        terminal.printf(x, y, char)


def draw_v(x, y0, y1, char):
    for y in range(y0, y1):
        terminal.printf(x, y, char)


def draw_h(x0, x1, y, char):
    for x in range(x0, x1):
        terminal.printf(x, y, char)


def draw_rect(x, y, width, height, char):
    draw_h(x, x + width, y, char)
    draw_h(x, x + width, y + height - 1, char)

    draw_v(x, y, y + height, char)
    draw_v(x + width - 1, y, y + height, char)


def draw(x, y, char):
    terminal.printf(x, y, char)


def get_height():
    size = terminal.get("window.size")
    size_arr = size.split("x")
    h = int(size_arr[1])
    return h


def get_width():
    size = terminal.get("window.size")
    size_arr = size.split("x")
    w = int(size_arr[0])
    return w


def draw_line_rect(x, y, width, height, thickness):
    h_left = x + 1
    v_top = y + 1
    r = x + width - 1
    b = y + height - 1

    lines = get_lines(thickness)

    draw(x, y, lines[Line.U_L])
    draw_h(h_left, r, y, lines[Line.H])
    draw(r, y, lines[Line.U_R])

    draw(x, b, lines[Line.B_L])
    draw_h(h_left, r, b, lines[Line.H])
    draw(r, b, lines[Line.B_R])

    draw_v(x, v_top, b, lines[Line.V])
    draw_v(r, v_top, b, lines[Line.V])
