from line_breaker import LineBreaker


class TxtPanel:
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.text = ""
        self.lines = list()

    def _break_lines(self):
        self.lines.clear()

        self.lines = LineBreaker().break_lines(self.text, self.w)

    def append(self, text):
        self.text += text

    def clear(self):
        self.lines.clear()

    def update(self):
        self._break_lines()

    def first_visible_idx(self):
        if len(self.lines) > self.h:
            return len(self.lines) - self.h

        return 0

    def get_visible_lines(self):
        start_idx = self.first_visible_idx()
        return self.lines[start_idx:len(self.lines)]

    def draw(self):
        lines = self.get_visible_lines()
        for line in lines:
            print(line)
