class LineBreaker:
    def __init__(self):
        self.start_idx = -1
        self.idx = -1
        self.text = ""
        self.lines = list()
        self.cols = 0

    def _eol(self):
        return self.idx >= len(self.text)

    def _pos_beyond(self):
        return self.idx - self.start_idx >= self.cols

    def _stretching_full_line(self):
        return self.break_idx == -1

    def _append_line(self):
        lne = self.text[self.start_idx:self.break_idx]
        self.lines.append(lne)

    def _append_full_line(self):
        lne = self.text[self.start_idx:self.start_idx + self.cols]
        self.lines.append(lne)

    def _start_new_line(self):
        self.start_idx = self.idx
        self.break_idx = -1

    def _move_to_non_ws_after_break(self):
        self.idx = self.break_idx
        while not self._eol() and not self.text[self.idx].isspace():
            self._advance()

    def _curr_pos_appropriate_for_breaK(self):
        chr = self.text[self.idx]
        if chr.isspace():
            return True

        puncts = list(["!", ".", ",", ";", "?"])
        if (chr in puncts):
            return True

        return False

    def _store_break_pos_if_appropriate(self):
        if(self._curr_pos_appropriate_for_breaK()):
            self.break_idx = self.idx + 1

    def _advance(self):
        self.idx += 1

    def break_lines(self, txt, cols):
        self.idx = 0
        self.start_idx = 0
        self.break_idx = -1
        self.cols = cols
        self.text = txt
        self.lines = list()

        while not self._eol():
            if self._pos_beyond():
                if self._stretching_full_line():
                    self._append_full_line()
                    self._start_new_line()
                else:
                    self._append_line()
                    self._move_to_non_ws_after_break()
                    self._start_new_line()
            else:
                self._store_break_pos_if_appropriate()

            self._advance()

        return self.lines
