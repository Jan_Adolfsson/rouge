from tokenizer import TokenType, Token, Tokenizer
from sentence import Sentence
from interpreter import Interpreter


def load(path):
    lines = list()
    with open(path, "r") as f:
        lines = f.readlines()

    return lines


def loadVerbs():
    verbs = {}
    verbs[0] = ["walk", "go"]
    return verbs


def loadNouns():
    nouns = {}
    nouns[0] = ["north", "n"]
    return nouns


def loadAdjectives():
    adjectives = {}
    return adjectives


def loadPrepositions():
    prepositions = {}
    return prepositions


def loadSentences():
    sentences = {}
    sentences[0] = Sentence(0, 0, None, None, 0, "north")
    return sentences


if (__name__ == "__main__"):
    lines = load("./data.txt")
    verbs = loadVerbs()
    nouns = loadNouns()
    adjs = loadAdjectives()
    preps = loadPrepositions()
    cmds = loadSentences()

    print("VERBS")
    print(verbs)

    print("NOUNS")
    print(nouns)

    print("ADJECTIVES")
    print(adjs)

    print("PREPOSITIONS")
    print(preps)

    print("SENTENCES")
    print(cmds)

    interpreter = Interpreter(verbs, nouns, adjs, preps, cmds)

    inp = input(">")
    while (inp != "q"):
        interpreter.interpret(inp)
        inp = input(">")
