from enum import Enum


class TokenType(Enum):
    UNKNOWN = 0
    VERB = 1
    NOUN = 2
    ADJ = 3
    PREP = 4
    CMD = 5


class Token:
    def __init__(self, tokenType, value):
        self._tokenType = tokenType
        self._value = value

    def get_type(self):
        return self._tokenType

    def get_value(self):
        return self._value

    def __repr__(self):
        return "tokenType: {}, value: {}".format(self._tokenType, self._value)