import collections
from tokenizer import Tokenizer
from token import Token, TokenType
from command import Command

class LineParser:
    def __init__(self, cmds):
        self._cmds = cmds


    def __init_parsing(self, tokens):
        self._tokens = tokens
        self._idx = 0


    def __curr_is_verb(self):
        return self._tokens[_idx].getType() == TokenType.VERB


    def __adv_idx(self):
        self._idx += 1


    def __parse_verb(self):
        self.__adv_idx()


    def __get_by_token(self, token_type):
        return list(filter(lambda t: t.get_type() == token_type, self._tokens))


    def __get_cmd_by_verb(self, verb_id):
        return list(filter(lambda c: c.get_verb() == verb_id))


    def __get_cmd_by_verb_and_nound(self, verb_id, noun_id):
        return list(filter(lambda c: c.get_verb() == verb_id and c.get_nound() == nound_id))


    def __parse_error(self):
        return Command()


    def parse(self, tokens):
        self.__init_parsing(tokens)
        
        if (len(tokens) == 1):
            # parse sentence with only 1 word
            return

        # parse sentence with more than 1 word

        verbs = self.__get_by_token(TokenType.VERB)
        if (len(verbs) == 0):
            return self.__parse_error()

        if (len(verbs) > 1):
            return self.__parse_error()

        verb = verbs[0]

        cmds = self.__get_cmd_by_verb(verb.get_value())
        if (len(cmds) == 0):
            # try parse by noun
            return


        nouns = self.__get_by_token(TokenType.NOUN)
        if (len(nouns) == 0):
            return self.__parse_error()

        if (len(verbs) + len(nouns) == len(tokens)):
            cmd = self.__get_cmd_by_verb_and_noun(verb.get_value(), noun.get_value())
            if (cmd == None):
                return self.__parse_error()

            return cmd

        return self.__parse_error()