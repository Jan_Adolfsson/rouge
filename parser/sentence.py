class Sentence:
    def __init__(self, id, verbId, prepId, adjId, nounId, action):
        self.id = id
        self.verbId = verbId
        self.prepId = prepId
        self.adjId = adjId
        self.nounId = nounId
        self.action = action

    def get_verb_id(self):
        return self.verbId

    def get_noun_id(self):
        return self.nounId

    def __repr__(self):
        return "id:{}, verbId:{}, prepId:{}, adjId:{}, nounId:{}, action:{}".format(
            self.id, self.verbId, self.prepId, self.adjId, self.nounId, self.action)
