from tokenizer import Tokenizer
from token import Token, TokenType
from lineParser import LineParser

class Interpreter:
    def __init__(self, verbs, nouns, adjs, preps, cmds):
        self._verbs = verbs
        self._nouns = nouns
        self._adjs = adjs
        self._preps = preps
        self._cmds = cmds
        self._tokenizer = Tokenizer(self._verbs, self._nouns, self._adjs, self._preps, self._cmds)        

    def __parse(self, tokens):
        parser = LineParser(self._cmds)
        cmd = parser.parse(tokens)
        return cmd


    def interpret(self, txt):
        sentence_trimmed = txt.strip()
        words = sentence_trimmed.split()
        tokens = self._tokenizer.tokenize(words)
        tokens_filtered = list(filter(lambda t: t.get_type() !=
                              TokenType.UNKNOWN, tokens))
        
        cmd = self.__parse(tokens_filtered)
        cmd.execute()
