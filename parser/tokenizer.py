from token import Token, TokenType

class Tokenizer:
    def __init__(self, verbs, nouns, adjs, preps, cmds):
        self._verbs = verbs
        self._nouns = nouns
        self._adjs = adjs
        self._preps = preps
        self._cmds = cmds


    @staticmethod
    def __find(words, word):
        for k in words.keys():
            if (word in words[k]):
                return k

        return -1


    def __to_token(self, word):
        verbId = self.__find(self._verbs, word)
        if (verbId > -1):
            token = Token(TokenType.VERB, verbId)
            return token

        nounId = self.__find(self._nouns, word)
        if (nounId > -1):
            token = Token(TokenType.NOUN, nounId)
            return token

        prepId = self.__find(self._preps, word)
        if (prepId > -1):
            token = Token(TokenType.PREP, prepId)
            return token

        adjId = self.__find(self._adjs, word)
        if (adjId > -1):
            token = Token(TokenType.ADJ, adjId)
            return token

        return Token(TokenType.UNKNOWN, None)


    def tokenize(self, words):
        words = words

        tokenz = list()
        for word in words:
            token = self.__to_token(word)
            tokenz.append(token)

        return tokenz


    def __str__(self):
        return "verbs:{}, nouns:{}, adjs:{}, preps:{}, cmds:{}".format(
            self._verbs, self._nouns, self._adjs, self._preps, self._cmds)
